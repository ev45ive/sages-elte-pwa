// Generated using webpack-cli https://github.com/webpack/webpack-cli

const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const WorkboxWebpackPlugin = require("workbox-webpack-plugin");
const { WebpackManifestPlugin }  = require('webpack-manifest-plugin')

const isProduction = false //process.env.NODE_ENV == "production";

const stylesHandler = MiniCssExtractPlugin.loader;

const config = {
  mode: "development",
  entry: {
    main: "./src/index",
    // sw: "./src/sw.ts",
  },
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, "build"),
    //  // Add /* filename */ comments to generated require()s in the output.
    //  pathinfo: isEnvDevelopment,
    //  // There will be one main bundle, and one file per asynchronous chunk.
    //  // In development, it does not produce real files.
    //  filename: isEnvProduction
    //    ? 'static/js/[name].[contenthash:8].js'
    //    : isEnvDevelopment && 'static/js/bundle.js',
    //  // There are also additional JS chunk files if you use code splitting.
    //  chunkFilename: isEnvProduction
    //    ? 'static/js/[name].[contenthash:8].chunk.js'
    //    : isEnvDevelopment && 'static/js/[name].chunk.js',
    assetModuleFilename: 'static/media/[name].[hash][ext]',
  },
  devServer: {
    open: true,
    host: "localhost",
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "public/index.html",
    }),

    new WebpackManifestPlugin({
      writeToFileEmit: true
    }),

    new MiniCssExtractPlugin(),

    // Add your plugins here
    // Learn more about plugins from https://webpack.js.org/configuration/plugins/
  ],
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/i,
        loader: "ts-loader",
        exclude: ["/node_modules/"],
      },
      {
        test: /\.css$/i,
        use: [stylesHandler, "css-loader"],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
        type: "asset",
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 5,
            }
          },
        ],
      },

      // Add your rules for custom modules here
      // Learn more about loaders from https://webpack.js.org/loaders/
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
};

module.exports = () => {

  config.plugins.push(new WorkboxWebpackPlugin.InjectManifest({
    swSrc: './src/service-worker.ts',
    swDest: './public/service-worker.js'
  }))

  /* config.plugins.push(new WorkboxWebpackPlugin.GenerateSW({
    // additionalManifestEntries:[],
    // cleanupOutdatedCaches: true
    // clientsClaim:false,
    // directoryIndex:'index.html',
    // dontCacheBustURLsMatching: /\.\w{8}\./,
    // globDirectory: './public/'
    // globPatterns: ['**.jpg'],
    // inlineWorkboxRuntime:true 
    swDest: 'service-worker.js',
    mode: 'development',
    excludeChunks: ['sw'],
    importScripts: ['./sw.js'],
    runtimeCaching: [
      {
        urlPattern: '*.jpg',
        handler: 'CacheFirst',
        options: {
          expiration: {
            maxAgeSeconds: 60 * 24 * 30,
          },
          cacheName: 'images-cache'
        }
      }
    ]

  }));
 */
  return config;
};
