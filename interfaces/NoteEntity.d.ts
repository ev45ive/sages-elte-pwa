
interface GeoValue {
  latitude: number;
  longitude: number;
}

interface UserEntity {
  id: string;
  name: string;
  avatar?: string;
}

export interface NoteEntity {
  id: string,
  title: string,
  body: string,
  geo?: GeoValue,
  images?: string[]
  user?: UserEntity
  updatedAt: number
  createdAt: number
}