import React, { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Routes, Route } from "react-router";
import { NotesList } from "./notes/pages/NotesList";
import { NoteDetails } from "./notes/pages/NoteDetails";

function App() {
  useEffect(() => {
    navigator.serviceWorker.getRegistration().then((registration) => {


      Notification.requestPermission().then((result) => {
        console.log("Notifications :" + result);

        registration!.pushManager.subscribe({
          userVisibleOnly: true,
          applicationServerKey: "BMPpOojVs-h8TNuP5MtPzpnw-uVARRJOCrTIPPNEi6sJZBiLRWHJ0zcGnFjBdXSgQmmIagnYMrhFard2uGUoeKk",
        }).then(result => {
            // send keys to server
          
        })
      });
    });
  }, []);
  const notify = async () => {
    const reg = await navigator.serviceWorker.getRegistration();
    var notification = reg?.showNotification("To do list", {
      body: "HEY! Your task  is now overdue.",
      icon: "/to-do-notifications/img/icon-128.png",
      actions: [
        {
          action: "explore",
          title: "Explore this new world",
          icon: "images/checkmark.png",
        },
        {
          action: "close",
          title: "Close notification",
          icon: "images/xmark.png",
        },
      ],
    });
  };
  return (
    //  .container>.row>.col>h1.display-3{Notes App}
    <div className="container">
      <div className="row">
        <div className="col">
          <h1 className="display-3" onClick={notify}>
            Notes App
          </h1>

          <Routes>
            <Route path="/" element={<NotesList />} />
            <Route path="/create" element={<NoteDetails />} />
            <Route path="/:id" element={<NoteDetails />} />
          </Routes>
        </div>
      </div>
    </div>
  );
}

export default App;
