import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { GeoValue, NoteEntity } from "../../../interfaces/NoteEntity";

interface Props {
  note?: NoteEntity;
  onSubmit(note: NoteEntity): void;
}

const EMPTY_NOTE: NoteEntity = {
  id: "",
  title: "",
  body: "",
  updatedAt: 0,
  createdAt: 0,
};

export const NoteEditor = ({ note = EMPTY_NOTE, onSubmit }: Props) => {
  const [noteTitle, setnoteTitle] = useState(note.title);
  const [noteBody, setnoteBody] = useState(note.body);
  const [geo, setGeo] = useState<GeoValue | undefined>(note.geo);
  const imagesRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    const handler = (event: GeolocationPosition): void => {
      const { latitude, longitude } = event.coords;
      setGeo({ latitude, longitude });
    };
    navigator.geolocation.getCurrentPosition(handler, console.error, {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
    });

    const video = (window as any).video as HTMLVideoElement;
    const canvas = (window as any).canvas;
    const width = video.width;

    // navigator.mediaDevices
    //   .getUserMedia({ video: true, audio: false })
    //   .then(function (stream) {
    //     video.srcObject = stream;
    //     video.play();
    //   });

    video.addEventListener(
      "canplay",
      function (ev: any) {
        const height = video.videoHeight / (video.videoWidth / width);

        video.setAttribute("width", width.toString());
        video.setAttribute("height", height.toString());
        canvas.setAttribute("width", width);
        canvas.setAttribute("height", height);
      },
      false
    );
  }, []);

  const snapshot = () => {
    const canvas = (window as any).canvas;
    var context = canvas.getContext("2d");
    context.fillStyle = "#AAA";
    context.fillRect(0, 0, canvas.width, canvas.height);

    // canvas.toBlob()
    var data = canvas.toDataURL("image/png");
    (window as any).photo.setAttribute("src", data);
  };

  const uploadFiles = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const formData = new FormData();
    for (let file of event.target.files!) {
      formData.append("images", file, file.name);
    }
    const { data } = await axios.post(
      `http://localhost:9000/v1/notes/${note.id}/upload`,
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }
    );
    console.log("Uploaded", data);
  };

  useEffect(() => {
    setnoteTitle(note.title);
    setnoteBody(note.body);
  }, [note]);

  return (
    <div>
      NoteEditor
      <form
        onSubmit={(event) => {
          event.preventDefault();
          onSubmit({
            ...note,
            title: noteTitle,
            body: noteBody,
            geo,
          });
        }}
      >
        <div className="form-group mb-3">
          <label htmlFor="note_title">Title</label>
          <input
            type="text"
            className="form-control"
            name="note_title"
            id="note_title"
            aria-describedby="helpId"
            placeholder="Title"
            value={noteTitle}
            onChange={(e) => setnoteTitle(e.target.value)}
          />
          <small id="helpId" className="form-text text-muted">
            Note title
          </small>
        </div>

        <div className="form-group mb-3">
          <label htmlFor="note_text">Note text</label>
          <textarea
            className="form-control"
            name="note_text"
            id="note_text"
            rows={5}
            value={noteBody}
            onChange={(e) => setnoteBody(e.target.value)}
          ></textarea>
        </div>

        <pre>GEO : {JSON.stringify(geo)}</pre>

        <input
          type="file"
          name="images[]"
          id="images"
          multiple={true}
          ref={imagesRef}
          onChange={uploadFiles}
        />

        {note.images?.map((image) => (
          <img width="50" src={`http://localhost:9000/${image}`} />
        ))}

        <button type="submit" className="btn btn-primary">
          Submit
        </button>

        <br />
        <video id="video" width="100%" />

        <canvas id="canvas" />

        <div className="output">
          <img id="photo" alt="The screen capture will appear in this box." />
        </div>
        <button onClick={snapshot}>Snap</button>
      </form>
    </div>
  );
};
