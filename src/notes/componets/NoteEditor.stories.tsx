import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { NoteEditor } from "./NoteEditor";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Notes/NoteEditor",
  component: NoteEditor,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    // backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof NoteEditor>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof NoteEditor> = (args) => (
  <NoteEditor {...args} />
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  note: {
    id: "123",
    title: "Tesst note",
    body: "Tesst note",
    createdAt:0,
    updatedAt:0
  },
};
