import React from "react";
import { Link } from "react-router-dom";
import { NoteEntity } from "../../../interfaces/NoteEntity";

interface Props {
  note: NoteEntity;
}

export const NoteListItem = ({ note }: Props) => {
  return (
    <div className="list-group-item">
      {note.title}
      <Link className="float-end" to={note.id}>
        Details
      </Link>
    </div>
  );
};
