import axios from "axios";
import React, { useEffect, useState } from "react";
import { NoteEntity } from "../../../interfaces/NoteEntity";
import { NoteListItem } from "../componets/NoteListItem";
import { useNavigate } from "react-router";
import { getAllNotes, getDb, updateAllNotes } from "../../core/nodesDb";

// const notes: NoteEntity[] = [
//   {
//     id: "123",
//     title: "Note 123",
//     body: "Note 123 body",
//   },
//   {
//     id: "234",
//     title: "Note 234",
//     body: "Note 234 body",
//   },
//   {
//     id: "345",
//     title: "Note 345",
//     body: "Note 345 body",
//   },
// ];
const CACHE_NOTES_LIST_ID = "notes-list";

export const NotesList = () => {
  const [notes, setnotes] = useState<NoteEntity[]>([]);
  const [refreshToken, setrefreshToken] = useState(Date.now());
  const navigate = useNavigate();

  useEffect(() => {
    // setnotes(JSON.parse(localStorage.getItem(CACHE_NOTES_LIST_ID) || "[]"));
    (async () => {
      const results = await getAllNotes();
      setnotes(results);
    })();
  }, []);

  useEffect(() => {
    axios.get("http://localhost:9000/v1/notes").then((res) => {
      setnotes(res.data);
      updateAllNotes(res.data)
    });
  }, [refreshToken]);

  return (
    <div>
      <div>
        <ul className="list-group mb-5">
          {notes.map((note) => (
            <NoteListItem note={note} key={note.id} />
          ))}
        </ul>
      </div>

      <button
        type="button"
        className="btn btn-secondary"
        onClick={() => setrefreshToken(Date.now())}
      >
        Refresh
      </button>
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => navigate("/create")}
      >
        Create new
      </button>
    </div>
  );
};