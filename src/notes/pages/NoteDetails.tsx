import React, { useEffect, useState } from "react";
import { NoteEditor } from "../componets/NoteEditor";
import { Link, useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import { NoteEntity } from "../../../interfaces/NoteEntity";
import { getNoteById, syncNote, updateNote } from "../../core/nodesDb";

export const NoteDetails = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [message, setmessage] = useState("");
  const [note, setnote] = useState<NoteEntity | undefined>(undefined);
  const [refreshToken, setrefreshToken] = useState(Date.now());

  useEffect(() => {
    if (!id) return;

    getNoteById(id).then((cachedNote) => cachedNote && setnote(cachedNote))
    .catch(err => {
      navigator.serviceWorker.getRegistration().then(reg => {
        (reg as any).sync('sync-notes')
      })
    })

    axios.get(`http://localhost:9000/v1/notes/${id}`).then((res) => {
      setnote(res.data);
      updateNote(res.data);
    });
  }, [refreshToken, id]);

  const saveNote = async (draft: NoteEntity) => {
    let res;
    try {
      if (draft.id) {
        res = await axios.put(
          `http://localhost:9000/v1/notes/${draft.id}`,
          draft
        );
        setmessage("Note updated");
      } else {
        res = await axios.post(`http://localhost:9000/v1/notes/ `, draft);
        draft.id = res.data.id;
        setmessage("Note created");
      }
    } finally {
      await syncNote(draft);
    }

    // setnote(res.data);
    // setrefreshToken(Date.now());
    navigate("/" + draft.id, { replace: true }); // redirect after submit
  };

  return (
    <div>
      <Link className="float-end" to="/">
        Back
      </Link>
      <h3 className="mb-3">NoteDetails</h3>
      {false} {null} {true} {undefined}
      {message && <p className="alert alert-info">{message}</p>}
      <NoteEditor note={note} onSubmit={saveNote} />
    </div>
  );
};
