import { NoteEntity } from './../../interfaces/NoteEntity.d';

// import { IDBPDatabase, openDB, } from 'idb'
import { openDB, IDBPDatabase } from 'idb/with-async-ittr';
import axios from 'axios';

let db: Promise<IDBPDatabase>

export async function getDb() {
    interface NodesSchema {
        key: NoteEntity['id'];
        indexes: {
            by_title: string;
            by_body: string;
            by_created: number;
            by_updated: number;
        };
        value: NoteEntity & Partial<{
            createdAt: number;
            updatedAt: number;
        }>;
    }

    return openDB<{
        notes: NodesSchema,
        syncnotes: {
            key: NoteEntity['id'];
            value: NoteEntity & { localUUID: boolean }
        },
    }>('notes-db', 2, {
        upgrade(db, old, newDb, tx) {
            if (old === newDb) return;

            // import('./migrations/vs.js')

            if (old === 0) {
                const store = db.createObjectStore('notes', {
                    // autoIncrement:false,
                    keyPath: 'id'
                })
                store.createIndex('by_title', 'title', {})
                store.createIndex('by_body', 'body', {})
                store.createIndex('by_created', 'createdAt', {})
                store.createIndex('by_updated', 'updatedAt', {})

                // store.put({
                //     id: "123",
                //     title: "Note 123",
                //     body: "Note 123 body",
                //     createdAt: Date.now()
                // })
                old = 1;
            }
            if (old === 1) {
                db.createObjectStore('syncnotes', {
                    keyPath: 'id'
                })
            }
        },
        blocked() {
            console.log('blocked');
        },
        blocking() {
            console.log('blocking');
        },
        terminated() {
            console.log('terminated');
        }
    })
}


export async function getNoteById(id: NoteEntity['id']) {
    const db = await getDb();
    return db.get('notes', id)
}

export async function updateNote(note: NoteEntity) {
    const db = await getDb();
    return db.put('notes', note)
}
export async function syncNote(draft: NoteEntity) {
    const db = await getDb();
    if (!draft.id) {
        draft.id = crypto.randomUUID();
        await db.put('syncnotes', {
            ...draft, localUUID: true
        })
    } else {
        await db.put('syncnotes', {
            ...draft, localUUID: false
        })
    }
    await db.put('notes', draft)
    return draft;
}

window.addEventListener('online', async () => {
    const db = await getDb();
    const store = db.transaction('syncnotes').store;
    const results = await store.getAll()

    for await (const note of results) {
        try {
            if (note.localUUID) {
                await axios.post(`http://localhost:9000/v1/notes/ `, note);
            } else {
                await axios.put(`http://localhost:9000/v1/notes/${note.id}`, note)
            }
            await db.delete('syncnotes', note.id)

        } catch (err) { }
    }
})

export async function getAllNotes() {
    const db = await getDb();
    // const tx = db.transaction("notes");
    // const results = [];
    // for await (const cursor of tx) {
    //     results.push(cursor.value);
    // }
    // const tx = db.transaction('store').store
    // const results = await db.getAllFromIndex('notes', 'by_created')
    const results = await db.transaction('notes').store.getAll()

    return results;
}


export async function updateAllNotes(notes: NoteEntity[]) {
    const db = await getDb();
    const tx = db.transaction('notes', 'readwrite')
    await tx.store.clear()

    for await (const note of notes) {
        await tx.store.put(note)
    }
}

