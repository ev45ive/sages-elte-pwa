/// <reference no-default-lib="true"/>
/// <reference types="serviceworker" />
/// <reference lib="ES2020" />


addEventListener('install', function (event) {
    // event.waitUntil()
    console.log('install');
    skipWaiting()
})

addEventListener('activate', function (event) {
    console.log('activate');
    clients.claim()
})


addEventListener('fetch', async function (event) {
    // https://developer.mozilla.org/en-US/docs/Web/API/Request/destination
    // https://developer.mozilla.org/en-US/docs/Web/API/Request/mode

    console.log('fetch ', event.request.mode, event.request.destination, event.request.url);

    // event.respondWith(await fetch(event.request)) 

})

