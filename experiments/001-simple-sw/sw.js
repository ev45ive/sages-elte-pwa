
/// <reference no-default-lib="true"/>
/// <reference types="serviceworker"/>
/// <reference lib="es2020"/>

console.log('SW');

const version = 'v3';

// oninstall
// addEventListener('install')
// caches.open('core')
// caches.open('runtime')

addEventListener('install', function (event) {
    event.waitUntil((async () => {
        const cache = await caches.open('core')
        await cache.addAll([
            '/',
            'index.html',
            'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css'
        ])
        await skipWaiting() // activate now, dont wait for tabs to close
    })())
})

addEventListener('activate', function (event) {
    // clear old caches

    clients.claim() // When worker is first installed imidiately control fetches 
});

addEventListener('fetch', function (event) {
    // event.request.mode == 'navigate' // Load page 
    // event.request.mode == 'no-cors' // Opaque response
    console.log(`SW ${version}: ` + event.request.url);

    event.respondWith((async () => {
        const cacheResponse = await caches.match(event.request)

        if (!cacheResponse) {
            return fetchAndUpdate(event.request)
        }
        return cacheResponse
    })())
})

addEventListener('fetch', function (event) {
    debugger
})

function fetchAndUpdate(request) {
    return fetch(request)
        .then(async (response) => {
            const cache = await caches.open('runtime')
            if ([0/* opaque response */, 200].includes(response.status)) {
                await cache.put(request, response.clone())
            }
            return response
        }).catch(error => {
            return Response.error()
        })
}