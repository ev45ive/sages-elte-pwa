
// /// <reference no-default-lib="true"/>
/// <reference types="serviceworker"/>

import { precacheAndRoute } from 'workbox-precaching';

// @ts-ignore
const manifest = self.__WB_MANIFEST

precacheAndRoute(manifest);


import {StaleWhileRevalidate} from 'workbox-strategies';

// registerRoute(
//   ({url}) => url.pathname.startsWith('/images/avatars/'),
//   new StaleWhileRevalidate()
// );