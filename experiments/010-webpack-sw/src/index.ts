/// <reference path="./index.d.ts"/>

import logo from './logo192.png'
import { Workbox } from "workbox-window";

if ("serviceWorker" in navigator) {
    const wb = new Workbox("/sw.js");


    wb.addEventListener('controlling', () => {
        window.location.reload()
    })

    wb.register();

}



console.log("Hello World 2!", logo);