
navigator.serviceWorker.register("sw.js").then((reg) => {
    window.registration = reg;

    // (registration.installing || registration.waiting).onstatechange = (event) =>{}

    reg.onupdatefound = (event) => {
        update.classList.toggle('highlight', true)
        updateStatuses();
    };

    registration.installing && (registration.installing.onstatechange = (event) => {
        state.querySelector(
            ".value"
        ).innerText = ` ( ${event.target.state} ) `;
        updateStatuses();
    });

    navigator.serviceWorker.oncontrollerchange = (event) => {
        update.classList.toggle('highlight', false)
        oncontrollerchange.classList.toggle('highlight', true)
        updateStatuses();
    };
    updateStatuses();

    function updateStatuses() {
        installing.classList.toggle('highlight', registration.installing)

        setTimeout(() => {
            installing.classList.toggle('highlight', registration.installing)
            waiting.classList.toggle('highlight', registration.waiting)
            active.classList.toggle('highlight', registration.active)
            updateVersion()
        }, 1000)
    }

    function updateVersion() {
        if (!navigator.serviceWorker.controller) return;
        const messageChannel = new MessageChannel()
        messageChannel.port1.addEventListener('message', (event) => {
            console.log('Message from SW', event.data);
        })
        navigator.serviceWorker.controller.postMessage({
            type: 'VERSION'
        }, [messageChannel.port2])
        
        // navigator.serviceWorker.onmessage = console.log
    }
    
});

function testRequest() {
    response.innerText = ''
    fetch('/api.json').then(resp => resp.text()).then(text => {
        response.innerText = text
    })
}