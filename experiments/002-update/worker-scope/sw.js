/// <reference no-default-lib="true"/>
/// <reference types="serviceworker" />
/// <reference lib="ES2020" />

const version = 15;

addEventListener('install', function (event) {
    event.waitUntil(new Promise(resolve => {
        setTimeout(() => {
            resolve(new Response('Installing ' + version))
        }, 800)
    }))
    console.log('installed');
    // skipWaiting()
})

addEventListener('activate', function (event) {
    console.log('activated');
    // clients.claim()
})

addEventListener('message', async function (event) {
    console.log('message ' + event.data.type, event.data);
    if (event.data.type === "SKIP_WAITING") {
        // forces the waiting service worker to become the active service
        skipWaiting()
    }
    if (event.data.type === "CLAIM") {
        // When a service worker is initially registered, pages won't use it until they next load.
        clients.claim()
    }

    if (event.data.type === "RELOAD_ALL") {
        // TypeError: This service worker is not the client's active service worker.
        const tabs = await clients.matchAll({ /* includeUncontrolled: true, */ type: 'window' });
        tabs.forEach(tab => {
            tab.navigate(tab.url) // reload tab
        })
    }
    if (event.data.type === "VERSION") {
        event.ports[0].postMessage(version)
    }
})

addEventListener('fetch', async function (event) {
    console.log('Responding with worker version ' + version);

    if (event.request.url.match('/api.json')) {
        event.respondWith(new Promise(resolve => {
            setTimeout(() => {
                resolve(new Response('Responding with worker version ' + version))
            }, 1000)
        }))
    }
})

