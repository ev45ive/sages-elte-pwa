## GIT

git clone https://bitbucket.org/ev45ive/sages-elte-pwa.git sages-elte-pwa
cd sages-elte-pwa
npm install
npm start

## Instalacje

node -v
v16.13.1

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

docker -v
Docker version 20.10.10, build b485636

code -v
1.63.2
899d46d82c4c95423fb7e10e68eba52050e30ba3
x64

## Create react app

npx create-react-app --version
5.0.0

npx create-react-app "." --template typescript

## Manifest

https://www.pwabuilder.com/imageGenerator

## Screen mirroring

https://www.vysor.io/
https://adb.clockworkmod.com/

## Optimise unused CSS

https://purgecss.com/guides/react.html#use-craco

## Https

ngrok.com

./ngrok authtoken XXXXXXX
./ngrok http 3000

## DEvice

HTTPS=true & npm run start

Chrome -> https://twojadresIp:3000/ -> dziala?
Android -> https://twojadresIp:3000/ -> dziala?

chrome://inspect/#devices

Jesli tylko USB => port forwarding 3000 | localhost:3000

## Certyfikaty

https://medium.com/swlh/how-to-make-react-js-use-https-in-development-4ead560eff10

https://proxyman.io/posts/2020-09-29-Install-And-Trust-Self-Signed-Certificate-On-Android-11

Lepiej uzyc chrome://inspect Port Forwarding

# Fast action shortcuts

# Badges

https://badging-api.glitch.me/

## Notes App

<!-- mkdir src/pages/NotesList.tsx -->

mkdir -p src/notes/pages/
mkdir -p src/notes/componets/

touch src/notes/pages/NotesList.tsx
touch src/notes/pages/NoteDetails.tsx
touch src/notes/componets/NoteEditor.tsx
touch src/notes/componets/NoteEditor.stories.tsx
touch src/notes/componets/NoteListItem.tsx
touch src/notes/componets/NoteListItem.stories.tsx

## Xiaomi

jakby kto snie miał opcji debugowanie po wifi tak jak ja w Xiaomi to: \

C:\Users\UN\Downloads\platform-tools>adb tcpip 5555
C:\Users\UN\Downloads\platform-tools>adb connect 192.168.0.192
connected to 192.168.0.192:5555
dwie komendy i już działa bez kabla po wifi

## Using service workers as Mockup API for Testing

https://mswjs.io/docs/basics/request-handler
https://storybook.js.org/addons/msw-storybook-addon

### Webpack Dev Server proxy

https://create-react-app.dev/docs/proxying-api-requests-in-development/
https://webpack.js.org/configuration/dev-server/

### Override Create React App

https://github.com/gsoft-inc/craco/blob/master/packages/craco/README.md#configuration-file
https://github.com/timarney/react-app-rewired

### Service worker - WorkBox

https://developers.google.com/web/tools/workbox
https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin

## Simple server

npx http-server -p 8080

<!-- lub -->

npm i -g http-server
hs -p 8080

### Webpack CLI Init

npx webpack-cli init

? Which of the following JS solutions do you want to use? Typescript
? Do you want to use webpack-dev-server? Yes
? Do you want to simplify the creation of HTML files for your bundle? Yes
? Do you want to add PWA support? Yes
? Which of the following CSS solutions do you want to use? CSS only
? Will you be using PostCSS in your project? No
? Do you want to extract CSS for every file? Yes
? Pick a package manager: npm

[webpack-cli] ℹ INFO Initialising project...
create package.json
conflict src\index.ts
? Overwrite src\index.ts? overwrite this and all others
force src\index.ts
create README.md
create index.html
create webpack.config.js
create tsconfig.json

## Sensors

```js
window.addEventListener('devicemotion', ({rotationRate,acceleration})=>{
    console.log(rotationRate,acceleration)
})
``
```


## HTML5 APIS
https://whatwebcando.today/

https://hammerjs.github.io/

### Audio Video
https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Taking_still_photos#get_the_video