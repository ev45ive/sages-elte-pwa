/// <reference lib="webworker" />
/* eslint-disable no-restricted-globals */

console.log('Hello from service worker 3!');

// This code executes in its own worker or thread

self.addEventListener("install", event => {
    console.log("Service worker installed");
    self.skipWaiting()
});

self.addEventListener("activate", (event: ExtendableEvent) => {
    console.log("Service worker activated");

    event.waitUntil(self.clients.claim());
});