

import express from 'express';
import { notesRoute } from './notesRoute';
import cors from 'cors';
import fs from 'fs'


const app = express()
app.use(express.json())
app.use(express.static('./public/'))
app.use(cors({
    origin: '*'
}))

app.use((req, res, next) => {

    setTimeout(() => {
        next()
    }, 1000)
})

app.use('/v1/notes', notesRoute)

const PORT = 9000;

const server = app.listen(PORT, () => {
    console.log(`Listening on http://localhost:${PORT}/`);
})

