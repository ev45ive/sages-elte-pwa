import { NoteEntity } from './../../interfaces/NoteEntity';

import { Router } from 'express';
import { randomUUID } from 'crypto';

import multer from 'multer';
import fs from 'fs';
import webpush from "web-push";

// VAPID keys should be generated only once.
let vapidKeys: webpush.VapidKeys;
try {
    const data = fs.readFileSync('./vapidKeys.json')
    vapidKeys = JSON.parse(data.toString())

} catch (err) {
    vapidKeys = webpush.generateVAPIDKeys();
    fs.writeFileSync('./vapidKeys.json', JSON.stringify(vapidKeys, null, 2))
}

webpush.setVapidDetails(
    "mailto:example@yourdomain.org",
    vapidKeys.publicKey,
    vapidKeys.privateKey
);


var subscription = {
    "endpoint": "https://fcm.googleapis.com/fcm/send/cqB7Rvc1Igk:APA91bGtIGJLcQQixPoMX0EchZ4Uz31d-1n5nw9Lu_NtxMV7w2mYg98KXx7HcZdziN6xfjFV_5NBzhupNdNh9azKz__bnNEcGlDweyEIwzzEZU8pzpl2Hlrp_3sl5lkZdOzoLE2Weo-L",
    "expirationTime": null,
    "keys": {
        "p256dh": "BL17QyI3_iRseMoLGcqZW5FLWl7CBU_V7yezJIfqTiDemYxq76rcU-I9Ov5b2Xt57-XWqB2uEySIXcUa67stO94",
        "auth": "O2cLCoej6N0xuuiEUK9K_Q"
    }
}

const upload = multer({ dest: "./public/uploads/" });


export const notesRoute = Router();

const notesData: { items: NoteEntity[] } = {
    items: [
        {
            id: '123', title: 'test 123', body: 'testbody 123',
            updatedAt: 0, createdAt: 0
        }
    ]
}


notesRoute.post('/:noteId/upload', upload.array('images'), (req, res) => {

    const note = notesData.items.find(note => note.id === req.params.noteId)
    if (!note) {
        res.status(404).send({ message: 'Invalid note id' })
        return;
    }
    const files = req.files as Express.Multer.File[]

    note.images = (note.images || []).concat(files.map(file => '/uploads/' + file.filename))

    res.send(note)
})

notesRoute.get('/:id', (req, res) => {
    const found = notesData.items.find(n => n.id === req.params.id);
    if (!found) {
        res.status(404).send({ message: 'NotFound' })
    }
    res.send(found);
});

notesRoute.get('/', (req, res) => {
    res.send(notesData.items);
});

notesRoute.post('/', (req, res) => {
    const newItem = {
        ...req.body,
        id: req.body.id || randomUUID(),
        createdAt: Date.now(),
        updatedAt: Date.now(),
    };
    notesData.items.push(newItem)
    res.send(newItem);
});

notesRoute.put('/:id', (req, res) => {
    const foundIndex = notesData.items.findIndex(n => n.id === req.params.id);
    if (foundIndex === -1) {
        res.status(404).send({ message: 'NotFound' })
    }
    const newItem = {
        ...notesData.items[foundIndex],
        ...req.body,
        updatedAt: Date.now(),
    };
    notesData.items[foundIndex] = newItem;

    webpush.sendNotification(subscription, 'Note created', {

    })

    res.send(newItem);
});

notesRoute.delete('/:id', (req, res) => {
    notesData.items = notesData.items.filter(n => n.id !== req.params.id);
    res.send('hello');
});
