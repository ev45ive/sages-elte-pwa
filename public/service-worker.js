/// <reference types="serviceworker" />
/// <reference lib="ES2020" />

/* eslint-disable no-restricted-globals */

const version = 'v1'
const CORE_CACHE_NAME = "pwa-assets/" + version;
const RUNTIME_CACHE_NAME = "pwa-runtime-assets/" + version;


// const ASSETS_CACHE_NAME = "pwa-assets/v1" + version;
// const LAZY_CACHE_NAME = "pwa-assets/v1" + version;

// @ts-ignore
let filesToCache = self.__WB_MANIFEST

filesToCache = [
    { 'revision': null, 'url': '/index.html' },
    { 'revision': null, 'url': '/static/css/main.css' },
    { 'revision': null, 'url': '/static/js/main.js' }
]

const OFFLINE_URL = '/offline.html'

addEventListener("install", (event) => {

    event.waitUntil(async function buildCache() {
        const cache = await caches.open(CORE_CACHE_NAME);

        await cache.addAll(filesToCache.map(rev => rev.url + (rev.revision ? '?_rev=' + rev.revision : '')))

        // await Promise.all([
        //     cache.add("/"),
        //     cache.add("/index.html"),
        //     cache.addAll(["/static/js/bundle.js", "/favicon.ico"]),
        // ]);

        await cache.add(new Request(OFFLINE_URL, { cache: 'reload' }))

        console.log("Service cache built");
    }())

    console.log("Service worker installed");

    // Dont wait
    skipWaiting()
});

addEventListener('message', (msg) => {
    if (!msg.data.type) { return }
})

addEventListener("activate", async (event) => {
    console.log("Service worker activated");

    await self.clients.claim()

    for (let key of await caches.keys()) {
        // const cache = await caches.open(key)
        if (!key.includes(version)) await caches.delete(key)
    }
});

addEventListener("fetch", (event) => {
    console.log("Service worker handling request");

    console.log(event.request.url);

    event.respondWith((async () => {
        const coreCache = await caches.open(CORE_CACHE_NAME);
        try {
            // Serve cache first
            const runtimeCache = await caches.open(RUNTIME_CACHE_NAME);
            const cacheResponse = await caches.match(event.request, {
                ignoreSearch: true
            })

            if (!cacheResponse) {
                // No cache, fetch and cache
                return fetch(event.request).then(networkResponse => {
                    if (event.request.mode === 'navigate') {
                        runtimeCache.put(event.request, networkResponse.clone())
                    }
                    return networkResponse
                })
            } else {
                // Serve stale cache, but Revalidate in backgtound
                if (event.request.mode === 'navigate') {
                    runtimeCache.add(event.request)
                        .then(() => clients.get(event.clientId))
                        .then((client) => client && client.postMessage({
                            type: 'CACHE_UPATED', url: event.request.url
                        }))
                }
                return cacheResponse
            }
        } catch (error) {
            // Serve Offline fallback PAGE version 
            if (event.request.mode === 'navigate') {
                return (await coreCache.match(OFFLINE_URL)) || Response.error()
            }
            return Response.error()
        }
    })())
});


self.addEventListener("notificationclick", function (e) {
    var notification = e.notification;
    var action = e.action;

    if (action === "explore") {
        clients.openWindow('index.html')
    }
    if (action === "close") notification.close();
});

addEventListener('sync', event => {
    console.log('Fetch and cache some data');
    debugger
})

addEventListener('push', event => {

    event.waitUntil((async () => {
        const res = await event.data.text()

        var notification = registration.showNotification(res, {
            body: "HEY! Your task  is now overdue.",
            actions: [
                {
                    action: "explore",
                    title: "Explore this new world",
                    icon: "images/checkmark.png",
                },
                {
                    action: "close",
                    title: "Close notification",
                    icon: "images/xmark.png",
                },
            ],
        });
    })())
})